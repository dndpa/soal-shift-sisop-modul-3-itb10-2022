# soal-shift-sisop-modul-3-ITB10-2022


# Soal 1 
###### by Adinda Putri Audyna (5027201073) & Salman Al Farisi Sudirlan (5027201056)

## Soal

Novak meminta kepada anda untuk membuat program untuk memecahkan kode-kode di dalam file yang Ia simpan di drive dengan cara decoding dengan base 64. Agar lebih cepat, Ia sarankan untuk menggunakan thread.

a. Download dua file zip dan unzip file zip tersebut di dua folder yang berbeda dengan nama quote untuk file zip quote.zip dan music untuk file zip music.zip. Unzip ini dilakukan dengan bersamaan menggunakan thread.

b. Decode semua file .txt yang ada dengan base 64 dan masukkan hasilnya dalam satu file .txt yang baru untuk masing-masing folder (Hasilnya nanti ada dua file .txt) pada saat yang sama dengan menggunakan thread dan dengan nama quote.txt dan music.txt. Masing-masing kalimat dipisahkan dengan newline/enter.

c. Pindahkan kedua file .txt yang berisi hasil decoding ke folder yang baru bernama hasil.

d. Folder hasil di-zip menjadi file hasil.zip dengan password 'mihinomenest[Nama user]'. (contoh password : mihinomenestnovak)

e. Karena ada yang kurang, kalian diminta untuk unzip file hasil.zip dan buat file no.txt dengan tulisan 'No' pada saat yang bersamaan, lalu zip kedua file hasil dan file no.txt menjadi hasil.zip, dengan password yang sama seperti sebelumnya.

Catatan:
- Boleh menggunakan execv(), tapi bukan mkdir, atau system
- Jika ada pembenaran soal, akan di-update di catatan
- Kedua file .zip berada di folder modul
- Nama user di passwordnya adalah nama salah satu anggota kelompok, baik yang mengerjakan soal tersebut atau tidak. Misalkan satu kelompok memiliki anggota namanya Yudhistira, Werkudara, dan Janaka. Yang mengerjakan adalah Janaka. Nama passwordnya bisa mihinomenestyudhistira, mihinomenestwerkudara, atau mihinomenestjanaka
- Menggunakan apa yang sudah dipelajari di Modul 3 dan, kalau  perlu, di Modul 1 dan 2


## Penyelesaian

#### Deklarasi Library dan Pembuatan 2 Thread

Pada bagian awal ini phthread akan membuat suatu thread baru yang akan menjalankan fungsi yang nanti akan kita buat.
```c
#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/wait.h>

pthread_t tid[2];
int n_thread = 2;
pid_t child;
```

#### Pembuatan Fungsi Parser

Fungsi `parser` ini digunakan untuk memproses masing2 thread, yang mana disini kita bagi menjadi 2 thread, yaitu thread untuk mengelola data `music` dan thread untuk mengelola data `quote`.

```c
void* parser(void *arg)
{
	// Mendapatkan id dari thread.
	pthread_t id = pthread_self();

	// Lihat id thread.
	if (pthread_equal(id,tid[0]))
	{
		unzip("music.zip", "music");
		decode("music");
		move("music");
	}
	else if (pthread_equal(id,tid[1]))
	{
		unzip("quote.zip", "quote");
		decode("quote");
		move("quote");
	} else {
		printf("Thread tidak dikenali.\n");
	}

	return NULL;
}
```
Di dalam fungsi parser ini akan memanggil beberapa fungsi lainnya seperti fungsi unzip, decode, move yang akan dijelaskan si bawah ini.

#### Pembuatan Fungsi Unzip

Pada bagian ini terdapat fungsi `unzip` yang digunakan untuk melakukan extract suatu file zip. Disini kami menggunakan fungsi execv, yang mana memerlukan file name dan mengoutputkan di suatu dir name.

```c
void unzip(char *file_name, char* dir_name)
{
	// Variabel untuk argumen dari execv.
	char *argv[] = {"unzip", file_name, "-d", dir_name, NULL};

	// Execute unzip.
	child = fork();
	if (child==0) {
	  execv("/usr/bin/unzip", argv);
	}
	wait(NULL);
}
```

Contoh penerapannya yaitu yang terdapat di dalam fungsi parser tadi. Pada bagian ini akan dilakukan unzip pada file `music.zip` dan akan dikeluarkan di folder music.
```c
unzip("music.zip", "music");
```

#### Pembuatan Fungsi Zip

Pada bagian ini terdapat fungsi `zip` yang digunakan untuk melakukan zip suatu file. Pada bagian ini digunakan untuk meng-zip file hasil menjadi file `hasil.zip` dengan menambahkan password untuk mengaksesnya.

```c
void zip(char *dir_name, char* password)
{
	// Variabel untuk argumen dari execv.
	char *argv[] = {"zip", "--password", password, "-r", "hasil.zip", dir_name, NULL};

	// Execute zip.
	pid_t child = fork();
	if (child==0) {
	  execv("/usr/bin/zip", argv);
	}
	wait(NULL);
}

```

#### Pembuatan Fungsi Decode

Di bagian ini kami akan mendecode setiap file dengan base64 lalu akan mengirimkannya ke folder hasil dan file sesuai harta karun dengan commang base64 lalu jalankkan prossesnya.

```c
void decode(char* dir_name) {
  DIR *dir;
  struct dirent *entry;
  char *ext;

  char* genre[32];
  int num_genre = 0;

  // Open the directory.
  if (!(dir = opendir(dir_name)))
  {
    return;
  }

  // Read the directory entries.
  while ((entry = readdir(dir)) != NULL)
  {
    // Pass if the entry is a directory.
    if (entry->d_type == DT_DIR) {
      continue;
    }

    char* command = malloc(sizeof(char) * (128));
    sprintf(command, "(base64 -d ./%s/%s ; echo) >> ./%s/%s.txt ", dir_name, entry->d_name, dir_name, dir_name);

    pid_t child = fork();
    if (child==0) {
      execl("/bin/sh", "/bin/sh", "-c", command, NULL);
    } else {
      wait(NULL);
    }
  }

  closedir(dir);
}
```

#### Pembuatan Fungsi Move

setelah didecode jangan lupa hasilnya dipindahkan ke folder hasil dengan command "mv" pakai fork juga untuk melakukan prosesnya

```c
void move(char* dir_name) {
	// Buat sebuah perintah untuk memindahkan file hasil.
	char* command = malloc(sizeof(char) * (128));
	sprintf(command, "mv ./%s/%s.txt ./hasil/%s.txt ", dir_name, dir_name, dir_name, dir_name);

	pid_t child = fork();
	if (child==0) {
		execl("/bin/sh", "/bin/sh", "-c", command, NULL);
	} else {
		wait(NULL);
	}

}
```

#### Main Program

jadi di program ini kami membuat dulu folder hasilnya, lalu kita loop threadnya sesuai jumlah filenya. dalam loop kita unzip, decode, dan move degan fungsi parse. Setelah itu zip hasil foldernya dengan password yang diinginkan.

```c
int main(void)
{
	// Buat folder hasil jika belum ada.
	pid_t child = fork();
	if (child==0) {
		execl("/bin/sh", "/bin/sh", "-c", "mkdir -p hasil", NULL);
	} else {
		wait(NULL);
	}

	// Error variabel.
	int err;

	// Loop sejumlah thread.
	for (int i = 0; i < n_thread; i++)
	{
		// Unzip file.
		err = pthread_create(&(tid[i]),NULL,&parser,NULL); 
		// Check error.
		if(err != 0) {
			printf("\n Can't create thread : [%s]",strerror(err));
		}
	}

	// Tunggu semua thread selesai.
	pthread_join(tid[0],NULL);
	pthread_join(tid[1],NULL);

	// Zip hasil.
	zip("hasil", "password");
	
	exit(0);
	return 0;
}
```
## Kendala
Kendala kami yaitu kesulitan dalam mengerjakan no 2e.


# Soal 2

## Soal

Bluemary adalah seorang Top Global 1 di salah satu platform online judge. Suatu hari Ia ingin membuat online judge nya sendiri, namun dikarenakan Ia sibuk untuk mempertahankan top global nya, maka Ia meminta kamu untuk membantunya dalam membuat online judge sederhana. Online judge sederhana akan dibuat dengan sistem client-server dengan beberapa kriteria sebagai berikut:

a. Pada saat client terhubung ke server, terdapat dua pilihan pertama yaitu register dan login. Jika memilih register, client akan diminta input id dan passwordnya untuk dikirimkan ke server. Data input akan disimpan ke file users.txt dengan format username:password. Jika client memilih login, server juga akan meminta client untuk input id dan passwordnya lalu server akan mencari data di users.txt yang sesuai dengan input client. Jika data yang sesuai ditemukan, maka client dapat login dan dapat menggunakan command-command yang ada pada sistem. Jika tidak maka server akan menolak login client. Username dan password memiliki kriteria sebagai berikut:

- Username unique (tidak boleh ada user yang memiliki username yang sama)
- Password minimal terdiri dari 6 huruf, terdapat angka, terdapat huruf besar dan kecil

b. Sistem memiliki sebuah database pada server untuk menampung problem atau soal-soal yang ada pada online judge. Database ini bernama problems.tsv yang terdiri dari judul problem dan author problem (berupa username dari author), yang dipisah dengan \t. File otomatis dibuat saat server dijalankan.

c. Client yang telah login, dapat memasukkan command yaitu ‘add’ yang berfungsi untuk menambahkan problem/soal baru pada sistem. Saat client menginputkan command tersebut, server akan meminta beberapa input yaitu:

- Judul problem (unique, tidak boleh ada yang sama dengan problem lain)
- Path file description.txt pada client (file ini berisi deskripsi atau penjelasan problem)
- Path file input.txt pada client (file ini berguna sebagai input testcase untuk menyelesaikan problem)
- Path file output.txt pada client (file ini berguna untuk melakukan pengecekan pada submission client terhadap problem)

d. Client yang telah login, dapat memasukkan command ‘see’ yang berguna untuk menampilkan seluruh judul problem yang ada beserta authornya(author merupakan username client yang menambahkan problem tersebut). Format yang akan ditampilkan oleh server adalah sebagai berikut:

e. Client yang telah login, dapat memasukkan command ‘download <judul-problem>’ yang berguna untuk mendownload file description.txt dan input.txt yang berada pada folder pada server dengan nama yang sesuai dengan argumen kedua pada command yaitu <judul-problem>. Kedua file tersebut akan disimpan ke folder dengan nama <judul-problem> di client.

f. Client yang telah login, dapat memasukkan command ‘submit <judul-problem> <path-file-output.txt>’.  Command ini berguna untuk melakukan submit jawaban dari client terhadap problem tertentu. Algoritma yang dijalankan adalah client akan mengirimkan file output.txt nya melalui argumen ke 3 pada command, lalu server akan menerima dan membandingkan isi file output.txt yang telah dikirimkan oleh client dan output.txt yang ada pada folder dengan nama yang sesuai dengan argumen ke 2 pada command. Jika file yang dibandingkan sama, maka server akan mengirimkan pesan “AC” dan jika tidak maka server akan mengeluarkan pesan “WA”.

g. Server dapat menangani multiple-connection. Dimana jika terdapat 2 atau lebih client yang terhubung ke server, maka harus menunggu sampai client pertama keluar untuk bisa melakukan login dan mengakses aplikasinya.

Catatan:
- Dilarang menggunakan system() dan execv(). Silahkan dikerjakan sepenuhnya dengan thread dan socket programming. 
- Untuk download dan upload silahkan menggunakan file teks dengan ekstensi dan isi bebas (yang ada isinya bukan touch saja dan tidak kosong)
- Untuk error handling jika tidak diminta di soal tidak perlu diberi.

Struktur Direktori Client Server :

	├── Client
		├── client.c
	└── Server
		├── server.c



# Soal 3

## Soal

Nami adalah seorang pengoleksi harta karun handal. Karena Nami memiliki waktu luang, Nami pun mencoba merapikan harta karun yang dimilikinya berdasarkan jenis/tipe/kategori/ekstensi harta karunnya. Setelah harta karunnya berhasil dikategorikan, Nami pun mengirimkan harta karun tersebut ke kampung halamannya. Contoh jika program pengkategorian dijalankan:

	# Program soal3 terletak di /home/[user]/shift3/hartakarun
	$ ./soal3 
	# Hasilnya adalah sebagai berikut
	/home/[user]/shift3/hartakarun
		|-jpg
			|--file1.jpg
			|--file2.jpg
			|--file3.jpg
		|-c
			|--file1.c
		|-tar.gz
			|--file1.tar.gz

a. Hal pertama yang perlu dilakukan oleh Nami adalah mengextract zip yang diberikan ke dalam folder “/home/[user]/shift3/”. Kemudian working directory program akan berada pada folder “/home/[user]/shift3/hartakarun/”. Karena Nami tidak ingin ada file yang tertinggal, program harus mengkategorikan seluruh file pada working directory secara rekursif

b. Semua file harus berada di dalam folder, jika terdapat file yang tidak memiliki ekstensi, file disimpan dalam folder “Unknown”. Jika file hidden, masuk folder “Hidden”.

c. Agar proses kategori bisa berjalan lebih cepat, setiap 1 file yang dikategorikan dioperasikan oleh 1 thread.

d. Untuk mengirimkan file ke Cocoyasi Village, nami menggunakan program client-server. Saat program client dijalankan, maka folder /home/[user]/shift3/hartakarun/” akan di-zip terlebih dahulu dengan nama “hartakarun.zip” ke working directory dari program client.

e. Client dapat mengirimkan file “hartakarun.zip” ke server dengan mengirimkan command berikut ke server
	send hartakarun.zip

Karena Nami tidak bisa membuat programnya, maka Nami meminta bantuanmu untuk membuat programnya. Bantulah Nami agar programnya dapat berjalan!

Catatan:
- Kategori folder tidak dibuat secara manual, harus melalui program C
- Program ini tidak case sensitive. Contoh: JPG dan jpg adalah sama
- Jika ekstensi lebih dari satu (contoh “.tar.gz”) maka akan masuk ke folder dengan titik terdepan (contoh “tar.gz”)
- Dilarang juga menggunakan fork, exec dan system(), kecuali untuk bagian zip pada soal d

Struktur Direktori Client Server :

	├── Client
		├── client.c
		└── hartakarun.zip
	└── Server
		├── server.c
		└── hartakarun.zip





