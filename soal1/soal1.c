#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/wait.h>

pthread_t tid[2];
int n_thread = 2;
pid_t child;

void* parser(void *arg)
{
	// Mendapatkan id dari thread.
	pthread_t id = pthread_self();

	// Lihat id thread.
	if (pthread_equal(id,tid[0]))
	{
		unzip("music.zip", "music");
		decode("music");
		move("music");
	}
	else if (pthread_equal(id,tid[1]))
	{
		unzip("quote.zip", "quote");
		decode("quote");
		move("quote");
	} else {
		printf("Thread tidak dikenali.\n");
	}

	return NULL;
}

void unzip(char *file_name, char* dir_name)
{
	// Variabel untuk argumen dari execv.
	char *argv[] = {"unzip", file_name, "-d", dir_name, NULL};

	// Execute unzip.
	child = fork();
	if (child==0) {
	  execv("/usr/bin/unzip", argv);
	}
	wait(NULL);
}

void zip(char *dir_name, char* password)
{
	// Variabel untuk argumen dari execv.
	char *argv[] = {"zip", "--password", password, "-r", "hasil.zip", dir_name, NULL};

	// Execute zip.
	pid_t child = fork();
	if (child==0) {
	  execv("/usr/bin/zip", argv);
	}
	wait(NULL);
}

void decode(char* dir_name) {
  DIR *dir;
  struct dirent *entry;
  char *ext;

  char* genre[32];
  int num_genre = 0;

  // Open the directory.
  if (!(dir = opendir(dir_name)))
  {
    return;
  }

  // Read the directory entries.
  while ((entry = readdir(dir)) != NULL)
  {
    // Pass if the entry is a directory.
    if (entry->d_type == DT_DIR) {
      continue;
    }

    char* command = malloc(sizeof(char) * (128));
    sprintf(command, "(base64 -d ./%s/%s ; echo) >> ./%s/%s.txt ", dir_name, entry->d_name, dir_name, dir_name);

    pid_t child = fork();
    if (child==0) {
      execl("/bin/sh", "/bin/sh", "-c", command, NULL);
    } else {
      wait(NULL);
    }
  }

  closedir(dir);
}

void move(char* dir_name) {
	// Buat sebuah perintah untuk memindahkan file hasil.
	char* command = malloc(sizeof(char) * (128));
	sprintf(command, "mv ./%s/%s.txt ./hasil/%s.txt ", dir_name, dir_name, dir_name, dir_name);

	pid_t child = fork();
	if (child==0) {
		execl("/bin/sh", "/bin/sh", "-c", command, NULL);
	} else {
		wait(NULL);
	}

}

int main(void)
{
	// Buat folder hasil jika belum ada.
	pid_t child = fork();
	if (child==0) {
		execl("/bin/sh", "/bin/sh", "-c", "mkdir -p hasil", NULL);
	} else {
		wait(NULL);
	}

	// Error variabel.
	int err;

	// Loop sejumlah thread.
	for (int i = 0; i < n_thread; i++)
	{
		// Unzip file.
		err = pthread_create(&(tid[i]),NULL,&parser,NULL); 
		// Check error.
		if(err != 0) {
			printf("\n Can't create thread : [%s]",strerror(err));
		}
	}

	// Tunggu semua thread selesai.
	pthread_join(tid[0],NULL);
	pthread_join(tid[1],NULL);

	// Zip hasil.
	zip("hasil", "password");
	
	exit(0);
	return 0;
}
